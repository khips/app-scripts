/** multimethod.js
 The poor man's version of Clojure[Script] multimethods.
*/

function multimethod(dispatchFn, unknownFn) {
  if (!dispatchFn) {
    throw("You must supply a dispatch function.")
  }
  
  var methods = {}
  var fn = function() {
    var dispatchVal = dispatchFn.apply(null, arguments);
    var foundFn = methods[dispatchVal] || unknownFn;
    
    if (foundFn) {
      return foundFn.apply(null, arguments);
    } else {
      throw("Could not handle dispatch value: " + dispatchVal.toString());
    }
  }
  
  fn.methods = methods;
  fn.add = (function(val, f) {
    methods[val] = f;
  });
  
  fn.remove = function(val) {
    delete methods[val];
  };
  
  return fn;
}



function testMultimethod() {
  var myMM = multimethod(function(s) { return s.length; });
  
  myMM.add(1, function() { return "Length is one!" });
  myMM.add(2, function() { return "Length is two!" });
  
  Logger.log(myMM("a"));
  Logger.log(myMM("bc"));
  
  try {
    Logger.log(myMM("bcd"));
  } catch (er) {
    Logger.log("Got error: %s", er);
  }
}