/**

listToHtml( listSpecification ) -> HtmlOutput

Generates HTML from a list specification:
A fully specified element takes the form:
 * ["tagName#elID.classOne.classTwo" {attr: "value"} [... children ...]]

*/

// http://stackoverflow.com/questions/1787322/htmlspecialchars-equivalent-in-javascript
function escapeHtmlChars(s) {
  return s.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");
}

function mapToAttrsString(attrs) {
  var pieces = [];
  for (var k in attrs) {
    if (!attrs.hasOwnProperty(k))
      continue;
    
    pieces.push([k, "=\"", escapeHtmlChars(attrs[k]), "\""].join(""));
  }
  
  return pieces.join(" ");
}

function _listToHtml(list, output) {
  if (!Array.isArray(list)) {
    if (list === undefined)
      return;
      
    if (typeof list == "string") {
      output.appendUntrusted(list);
      return;
    } 
    
    return;
  }
  
  debugger;
  var tagSpec = list[0];
  var attrs = list[1];
  var children, tagName;
  
  if (attrs && attrs.constructor == Object) {
    children = list.slice(2);
  } else {
    attrs = {}
    children = list.slice(1);
  }
  
  var tagPieces = tagSpec.split(/(?=[.#])/g);
  var classes = (attrs["class"] && attrs["class"].split(/\s+/g)) || [];
  
  tagPieces.forEach(function(v) {
    if (v[0] == ".")
      classes.push(v.slice(1));
    else if (v[0] == "#")
      attrs["id"] = v.slice(1);
    else
      tagName = v;
  });
  
  // Default tag name?
  if (!tagName) tagName = "div";
  
  if (classes.length)
    attrs["class"] = classes.join(" ");
  
  output.append("<" + tagName).append(" ").append(mapToAttrsString(attrs));
  
  if (children.length) {
    output.append(">");
    children.forEach(function(child) {
      _listToHtml(child, output);
    });
  
    output.append("</" + tagName).append(">");
  } else {
    output.append("/>");
  }
}

function listToHtml(list) {
  var output = HtmlService.createHtmlOutput();
  _listToHtml(list, output);
  return output;
}


function test_mapToAttrsString() {
  Logger.log(mapToAttrsString({id: "myHeader", 
                               "class": "several classes", 
                               value: "<>", 
                               title: "\""}));
}

function test_listToHtml() {
  var out = listToHtml(["div.section.paragraph.title#mything", 
                        ["a", {href: "#"}, "Check this out!"],
                        ["img", {src: "image.jpg"}]]);
  Logger.log(out.getContent())
}