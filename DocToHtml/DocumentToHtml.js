var _ET = DocumentApp.ElementType;
var mmethod = Multimethods.multimethod;

function elementType(documentElt) {
  return documentElt.getType();
}

var ETTags = {};
ETTags[_ET.HORIZONTAL_RULE] = "hr";
ETTags[_ET.LIST_ITEM] = "li";
ETTags[_ET.PARAGRAPH] = "p";
ETTags[_ET.TABLE] = "table";
ETTags[_ET.TABLE_ROW] = "tr";
ETTags[_ET.TABLE_CELL] = "td";
ETTags[_ET.BODY_SECTION] = "div";


// listFromElement() multimethod
var listFromElement = mmethod(elementType, discardElement);

//listFromElement.add(_ET.LIST_ITEM, liToList);
listFromElement.add(_ET.INLINE_IMAGE, imgToList);
listFromElement.add(_ET.TEXT, textToList);
//listFromElement.add(_ET.PARAGRAPH, pToList);
//listFromElement.add(_ET.TABLE, tableToList);
//listFromElement.add(_ET.TABLE_ROW);
//listFromElement.add(_ET.TABLE_CELL);

// Default:
function discardElement(elt) {
  var tagname = ETTags[elt.getType()];
  if (tagname)
    return [tagname];
}

// Multimethod implementations for converting to a list.
function imgToList(img) {
  return ["img", {src: img.getLinkUrl()}];
}

function textToList(txt) {
  var runs = makeRuns(txt);
  return runsToHTML(runs);
}


// convertToType() multimethod
var convertToType = mmethod(elementType, function() { });

convertToType.add(_ET.BODY_SECTION, bodyToType);
convertToType.add(_ET.LIST_ITEM, liToType);
convertToType.add(_ET.INLINE_IMAGE, imgToType);
convertToType.add(_ET.TEXT, textToType);
convertToType.add(_ET.PARAGRAPH, pToType);
convertToType.add(_ET.TABLE, tableToType);
convertToType.add(_ET.TABLE_ROW, tableRowToType);
convertToType.add(_ET.TABLE_CELL, tableCellToType);

// Implementations:
function bodyToType(elt) {
  return elt.asBody();
}

function liToType(elt) {
  return elt.asListItem();
}

function imgToType(elt) {
  return elt.asInlineImage();
}

function textToType(elt) {
  return elt.asText();
}

function pToType(elt) {
  return elt.asParagraph();
}

function tableToType(elt) {
  return elt.asTable();
}

function tableRowToType(elt) {
  return elt.asTableRow();
}

function tableCellToType(elt) {
  return elt.asTableCell();
}


function elementToHtmlList(elt) {
  var typedElt = convertToType(elt);
  var baseList = listFromElement(typedElt);
  
  if (typedElt.getNumChildren && typedElt.getNumChildren() > 0) {
    var child = typedElt.getChild(0);
    while(child) {
      var lfe = elementToHtmlList(child);
      if (lfe && lfe.length) {
        baseList.push(lfe);
      }
      
      child = child.getNextSibling();
    }
  }
  
  return baseList;
}

/**
@returns {HtmlOutput}
*/
function elementToHtmlOutput(elt) {
	return listToHtml(elementToHtmlList(elt));
}