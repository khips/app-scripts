/**
@param o {Object} The object to copy
@returns {Object} A shallow copy of o
*/
function shallowCopy(o) {
  var newO = {};
  
  for (var k in o) {
    if (!o.hasOwnProperty(k)) 
      continue;
    
    newO[k] = o[k];
  }
  
  return newO;
}

var runStyleMap = {
  "isBold": $tag("strong"),
  "isItalic": $tag("em"),
  "isStrikethrough": $tag("strike"),
  "isUnderline": $tag("u"),
  "getFontSize": changeFontSize,
  "getLinkUrl": changeLink
}


function makeRuns(txtEl) {
  var text = txtEl.getText();
  var indices = txtEl.getTextAttributeIndices().concat([text.length-1]);

  return [[text.slice(0, indices[1])]].concat(Stride.stride(indices, 3, 1).map(function(idxs) {
    var before = idxs[0];
    var start = idxs[1];
    var end = idxs[2];
    
    var style = {};
    
    for (var k in runStyleMap) {
      if (!runStyleMap.hasOwnProperty(k))
        continue;
      
      var oldVal = txtEl[k](before);
      var newVal = txtEl[k](start);
      
      if (oldVal != newVal) {
        style[k] = [oldVal, newVal];
      }
    }
    
    return [text.slice(start, end), style];
  }));
}


function runsToHTML(runs) {
  // A stack of the current styles and the order in which
  // they were applied:
  var styleStack = [];
  
  return runs.map(function(run) {
    var style = shallowCopy(run[1]);
    var pieces = [];
    
    if (style) {
      var popStylesTo = styleStack.length;
      for (var i = 0, l = styleStack.length; i < l; ++i) {
        if (style[styleStack[i][0]]) {
          popStylesTo = i;
          break;
        }
      }
      
      var reapplyStyles = [];
      for (var i = styleStack.length-1; i >= popStylesTo; --i) {
        var pair = styleStack.pop();
        var k = pair[0];
        var oldVal = pair[1];
        pieces.push(runStyleMap[k](oldVal, null));
        // Insert at the front so that we reapply the styles
        // symmetrically.
        reapplyStyles.unshift(pair);
      }
      
      reapplyStyles.forEach(function(pair) {
        var k = pair[0];
        var newVal = style[k] ? style[k][1] : pair[1];
          
        pieces.push(runStyleMap[k](null, newVal));
        // Only apply the new style once.
        delete style[k];
        styleStack.push([k, newVal]);
      });
      
      for (var k in style) {
        var newVal = style[k][1];
        
        pieces.push(runStyleMap[k](null, newVal));
        styleStack.push([k, newVal]);
      }
    }
    
    pieces.push(run[0]);
    return pieces.join("");
  }).join("");;
}

function $tag(tag) {
  return function(oldVal, newVal) {
    var s = "";
    
    if (oldVal)
      s += "</" + tag + ">";
    
    if (newVal)
      s += "<" + tag + ">";
    
    return s;
  }
}

function changeFontSize(oldVal, newVal) {
  var s = "";
  
  if (oldVal)
    s += "</span>";
  
  if (newVal)
    s += "<span style=\"font-size:"  + newVal + "pt;\">";
  
  return s;
}

function changeLink(oldVal, newVal) {
  var s = "";
  
  if (oldVal)
    s += "</a>";
  
  if (newVal)
    s += "<a href=\"" + newVal + "\">";
  
  return s;
}